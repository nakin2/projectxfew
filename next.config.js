/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async rewrites () {
    return [
      {
        source: '/ads',
        destination: 'https://www.google.co.th/',
        basePath: false
      },
    ]}
}

module.exports = nextConfig
